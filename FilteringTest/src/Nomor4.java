import java.time.LocalDateTime;
import java.util.Scanner;

public class Nomor4 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Nomor4 n = new Nomor4();
		n.ubahFormatJam();
	}

	public void ubahFormatJam() {
		Scanner scan = new Scanner(System.in);
		System.out.println("Input: ");
		String s = scan.nextLine();
		int jam = 0;
		int menit = 0;
		String AMPM = "";
		if (s.contains("AM") || s.contains("PM")) {
			String[] a = s.split(" ");
			String jamMenit = a[0];
			jam = Integer.valueOf(jamMenit.substring(0, 2));
			menit = Integer.valueOf(jamMenit.substring(3, jamMenit.length()));
			if (a[1].equals("AM")) {
				if (jam == 12) {
					jam -= 12;
					if (menit < 10) {
						System.out.println("0" + jam + ".0" + menit);
					} else {
						System.out.println("0" + jam + "." + menit);
					}

				}else {
					if (menit < 10) {
						System.out.println("0" + jam + ".0" + menit);
					} else {
						System.out.println("0" + jam + "." + menit);
					}
				}
			} else {
				if (jam < 12) {
					jam += 12;
					if (menit < 10) {
						System.out.println(jam + ".0" + menit);
					} else {
						System.out.println(jam + ".0" + menit);
					}

				} else {
					if (menit < 10) {
						System.out.println(jam + ".0" + menit);
					} else {
						System.out.println(jam + ".0" + menit);
					}
				}
			}
		} else {
			jam = Integer.valueOf(s.substring(0, 2));
			menit = Integer.valueOf(s.substring(3, s.length()));
			if (jam > 12) {
				jam -= 12;
				if (jam < 10) {
					AMPM = "PM";
					if (menit < 10) {
						System.out.println("0" + jam + ".0" + menit + " " + AMPM);
					} else {
						System.out.println("0" + jam + "." + menit + " " + AMPM);
					}

				}

			} else {
				if (jam < 10) {
					AMPM = "AM";
					if (menit < 10) {
						System.out.println("0" + jam + ".0" + menit + " " + AMPM);
					} else {
						System.out.println("0" + jam + "." + menit + " " + AMPM);
					}

				}

			}

		}
	}
}
