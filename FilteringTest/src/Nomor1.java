import java.util.Scanner;

public class Nomor1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Nomor1 n = new Nomor1();
		n.panjangDeret();
	}

	public void panjangDeret() {
		Scanner scan = new Scanner (System.in);
		System.out.println("Masukkan Panjang Deret: ");
		int n = scan.nextInt();
		int kel3 = 3;
		int kelMin2 = -2;
		int[] kel3Arr = new int[n];
		int[] min2Arr = new int[n];

		for (int i = 0; i < n; i++) {
			kel3Arr[i] = kel3 - 1;
			kel3 += 3;
		}

		for (int i = 0; i < n; i++) {
			min2Arr[i] = kelMin2 * 1;
			kelMin2 -= 2;
		}
		
		for (int i = 0; i < min2Arr.length; i++) {
			if (i > 0 && (min2Arr[i] + kel3Arr[i]) % 3 == 0) {
				System.out.print("* ");
			} else if(i == 0 || (min2Arr[i] + kel3Arr[i]) % 3 != 0) {
				System.out.print(min2Arr[i] + kel3Arr[i] + " ");
			}

		}
		
		scan.close();

	}
}
