import java.util.ArrayList;
import java.util.Scanner;

public class Nomor9 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Nomor9 n = new Nomor9();
		n.fibbonaci();
	}
	
	public void fibbonaci() {
		Scanner scan = new Scanner (System.in);
		System.out.println("Masukkan Nilai : ");
		int n = scan.nextInt();
		int temp = 0;
		ArrayList<Integer> listFib = new ArrayList<Integer>();
		for (int i = 0; i < n; i++) {
			if (i == 0) {
				listFib.add(i);
			} else if (i == 1){
				listFib.add(i);
			}else {
				temp = listFib.get(i-1)+ listFib.get(i-2);
				listFib.add(temp);
			}
		}
		
		int count = 0;
		for (int i = 0; i < listFib.size(); i++) {
			if (listFib.get(i) < n) {
				if(i!= 0 && listFib.get(i) % 2 == 0) {
					count +=1;
				}
			}
		}
		
		System.out.println("Jumlah Angka Genap = "+ count);
	}

}
