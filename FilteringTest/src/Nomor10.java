import java.util.Arrays;
import java.util.Scanner;

public class Nomor10 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Nomor10 n = new Nomor10();
		n.vokalKonsonan();
	}

	public void vokalKonsonan() {
		Scanner scan = new Scanner (System.in);
		System.out.println("Input : ");
		String str = scan.nextLine();
		String v = "";
		String k = "";
		str.trim();
		for (int i = 0; i < str.length(); i++) {
			if (str.charAt(i) == 'a' || str.charAt(i) == 'i' || str.charAt(i) == 'u' || str.charAt(i) == 'e'
					|| str.charAt(i) == 'o') {
				v += String.valueOf(str.charAt(i)).trim();
			} else {
				k += String.valueOf(str.charAt(i)).trim();
			}
		}
		System.out.println(v);
		System.out.println(k);
	
		char[] arrVoc = v.toCharArray();
		char[] arrKon = k.toCharArray();
		Arrays.sort(arrVoc);
		String a = new String(arrVoc);
		Arrays.sort(arrKon);
		String b = new String(arrKon);
		
		System.out.println(str);
		System.out.println("Vokal : " + a.toLowerCase());
		System.out.println("Konsonan: " + b.toLowerCase());
	}
}
